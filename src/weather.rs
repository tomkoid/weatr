use std::collections::HashMap;

use crate::args::Get;

pub fn get_weather(args: Get) {
    let mut params = HashMap::new();

    params.insert("latitude", args.latitude);
    params.insert("longitude", args.longitude);

    // optional params
    if args.current.is_some() {
        params.insert("current", args.current.unwrap_or("".to_string()));
    }

    if args.hourly.is_some() {
        params.insert("hourly", args.hourly.unwrap_or("".to_string()));
    }

    if args.wind_speed_unit.is_some() {
        let wind_speed_unit = args.wind_speed_unit.unwrap();

        if wind_speed_unit != "ms"
            && wind_speed_unit != "mph"
            && wind_speed_unit != "kh"
            && wind_speed_unit != "kn"
        {
            println!(
                "Invalid wind speed unit: {}\nAvailable units: ms, mph, kh, kn",
                wind_speed_unit
            );
            std::process::exit(1);
        }

        params.insert("wind_speed_unit", wind_speed_unit);
    }

    if args.temperature_unit.is_some() {
        let temperature_unit = args.temperature_unit.unwrap();

        if temperature_unit != "celsius" && temperature_unit != "fahrenheit" {
            println!(
                "Invalid temperature unit: {}\nAvailable units: celsius, fahrenheit",
                temperature_unit
            );
            std::process::exit(1);
        }

        params.insert("temperature_unit", temperature_unit);
    }

    if args.precipation_unit.is_some() {
        let precipation_unit = args.precipation_unit.unwrap();

        if precipation_unit != "mm" && precipation_unit != "inch" {
            println!(
                "Invalid precipation unit: {}\nAvailable units: mm, inch",
                precipation_unit
            );
            std::process::exit(1);
        }
    }

    if args.timeformat.is_some() {
        let timeformat = args.timeformat.unwrap();

        if timeformat != "unixtime" && timeformat != "iso8601" {
            println!(
                "Invalid timeformat: {}\nAvailable formats: iso8601, unixtime",
                timeformat
            );
            std::process::exit(1);
        }

        params.insert("timeformat", timeformat);
    }

    if args.timezone.is_some() {
        params.insert("timezone", args.timezone.unwrap());
    }

    // requests
    let client = reqwest::blocking::Client::new();

    let weather_request = client
        .get("https:///api.open-meteo.com/v1/forecast")
        .query(&params)
        .send()
        .unwrap();

    if args.output_raw {
        println!("{}", weather_request.text().unwrap());
        std::process::exit(0);
    }

    let json = weather_request.json::<serde_json::Value>().unwrap();

    if json["error"].is_null() == false {
        eprintln!(
            "ERROR: {}",
            json["reason"]
                .as_str()
                .expect("Failed to parse error reason")
        );
        std::process::exit(1);
    }

    let current_temperature = json["current"]["temperature_2m"].as_f64().expect("No temp");
    let current_temperature_unit = json["current_units"]["temperature_2m"]
        .as_str()
        .expect("No temp unit");
    let current_time_json = json["current"]["time"].as_str().expect("No time");

    let current_time = chrono::NaiveDateTime::parse_from_str(current_time_json, "%Y-%m-%dT%H:%M")
        .unwrap()
        .format("%H:%M")
        .to_string();

    if args.output_compact {
        println!(
            "Today at {} is {}{}",
            current_time, current_temperature, current_temperature_unit
        );
        std::process::exit(0);
    }
}
