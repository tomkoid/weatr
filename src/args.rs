use clap::{Parser, Subcommand};

#[derive(Parser, Debug)]
#[command(author = "Tomkoid", version, about, long_about = None)]
pub struct Args {
    #[command(subcommand)]
    pub command: Option<Commands>,
}

#[derive(Subcommand, Clone, Debug)]
pub enum Commands {
    Get(Get),
}

#[derive(Parser, Debug, Clone)]
pub struct Get {
    pub latitude: String,
    pub longitude: String,

    #[arg(
        short,
        long,
        default_value = "temperature_2m,relative_humidity_2m,apparent_temperature,is_day,precipitation,rain,showers,snowfall,weather_code,cloud_cover,pressure_msl,surface_pressure,wind_speed_10m,wind_direction_10m,wind_gusts_10m"
    )]
    pub current: Option<String>,

    #[arg(
        long,
        default_value = "temperature_2m,relative_humidity_2m,dew_point_2m,rain,showers,snowfall,wind_speed_10m"
    )]
    pub hourly: Option<String>,

    /// Available units: celsius, fahrenheit
    #[arg(long)]
    pub temperature_unit: Option<String>,

    /// Available units: ms, mph, kh, kn
    #[arg(long)]
    pub wind_speed_unit: Option<String>,

    /// Available units: mm, inch
    #[arg(long)]
    pub precipation_unit: Option<String>,

    /// Available formats: iso8601, unixtime
    #[arg(long)]
    pub timeformat: Option<String>,

    /// Force set timezone
    #[arg(long)]
    pub timezone: Option<String>,

    /// Respond with raw JSON or error
    #[arg(long, default_value_t = false)]
    pub output_raw: bool,

    /// Respond with compact repsonse
    #[arg(long, default_value_t = false)]
    pub output_compact: bool,

    /// Respond with response good for waybar
    #[arg(long, default_value_t = false)]
    pub output_waybar: bool,
}
