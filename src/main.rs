use clap::{CommandFactory, Parser};
use weather::get_weather;

mod args;
mod weather;

fn main() {
    let args = args::Args::parse();

    match args.command {
        Some(args::Commands::Get(args)) => {
            get_weather(args);
        }
        None => {
            println!("No command provided");
            args::Args::command().print_help().unwrap();
            std::process::exit(1);
        }
    }
}
